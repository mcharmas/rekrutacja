import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ZadParser {

    @Test
    public void evaluatesExpressionsWithMultipleCalculations() throws Exception {
        assertEquals(56, evaluate("((5+2)*(4*2))"));
    }

    private int evaluate(String expression) {
        return 0;
    }

}
