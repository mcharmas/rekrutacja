import org.junit.Test;
import pl.example.TodoList;

import static org.junit.Assert.assertEquals;

public class ZadTodo {
    @Test
    public void addsItemToTodoList() throws Exception {
        TodoList todoList = new TodoList();

        todoList.addItem("new item");

        assertEquals(1, todoList.size());
    }
}
