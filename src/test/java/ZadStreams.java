import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ZadStreams {
    @Test
    public void findsDoubleOfFirstEvenElementGreaterThan3() throws Exception {
        List<Integer> input = Arrays.asList(1, 2, 3, 3, 5, 6, 7, 8);

        assertEquals(12, findDouble(input));
    }

    /**
     * Znjaduje podwojoną wartość pierwszego parzystego elementu większego od trzech
     */
    private int findDouble(List<Integer> input) {
        return 0;
    }
}
